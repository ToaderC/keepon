using System.Runtime.InteropServices;

namespace KeepOn
{
    public partial class Main : Form
    {
        private const int intervalSeconds = 60;
        private int offset = 5;
        private int currentSecond = 0;
        private string started = "Started";
        private string notStarted = "Not started";

        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        public Main()
        {
            InitializeComponent();

            labelStatus.Text = $"{notStarted} {intervalSeconds}";

            timer.Interval = 1000;
            timer.Tick += (sender, args) =>
            {
                currentSecond += 1;
                labelStatus.Text = $"{started} {(intervalSeconds - currentSecond + 1)}";

                if (currentSecond > intervalSeconds)
                {
                    Cursor.Position = new Point(Cursor.Position.X + offset, Cursor.Position.Y);
                    offset *= -1;
                    DoMouseClick();
                    currentSecond = 0;
                }
            };
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            timer.Start();
            labelStatus.Text = $"{started} {(intervalSeconds - currentSecond)}";
            this.TopMost = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            timer.Stop();
            currentSecond = 0;
            labelStatus.Text = $"{notStarted} {intervalSeconds}";
            this.TopMost = false;
        }

        private void DoMouseClick()
        {
            uint X = (uint)Cursor.Position.X;
            uint Y = (uint)Cursor.Position.Y;
            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, X, Y, 0, 0);
        }
    }
}
