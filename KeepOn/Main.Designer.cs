﻿namespace KeepOn
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnStart = new Button();
            btnStop = new Button();
            labelStatus = new Label();
            SuspendLayout();
            //
            // btnStart
            //
            btnStart.Location = new Point(12, 12);
            btnStart.Name = "btnStart";
            btnStart.Size = new Size(94, 29);
            btnStart.TabIndex = 0;
            btnStart.Text = "Start";
            btnStart.UseVisualStyleBackColor = true;
            btnStart.Click += btnStart_Click;
            //
            // btnStop
            //
            btnStop.Location = new Point(112, 12);
            btnStop.Name = "btnStop";
            btnStop.Size = new Size(94, 29);
            btnStop.TabIndex = 1;
            btnStop.Text = "Stop";
            btnStop.UseVisualStyleBackColor = true;
            btnStop.Click += btnStop_Click;
            //
            // labelStatus
            //
            labelStatus.Location = new Point(12, 44);
            labelStatus.Name = "labelStatus";
            labelStatus.Size = new Size(196, 26);
            labelStatus.TabIndex = 2;
            labelStatus.Text = "Not started";
            labelStatus.TextAlign = ContentAlignment.MiddleCenter;
            //
            // Main
            //
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(220, 75);
            Controls.Add(labelStatus);
            Controls.Add(btnStop);
            Controls.Add(btnStart);
            Name = "Main";
            ShowIcon = false;
            ResumeLayout(false);

        }

        #endregion

        private Button btnStart;
        private Button btnStop;
        private Label labelStatus;
    }
}
